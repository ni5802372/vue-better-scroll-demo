import Vue from 'vue';
import Router from 'vue-router';
import HomePage from '@/views/home/base.vue';


Vue.use(Router);

const router = new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage,
    },
  ],
})

export default router;
