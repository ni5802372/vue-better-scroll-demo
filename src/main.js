import Vue from 'vue'
import App from './App.vue'
import 'normalize.css/normalize.css'
import router from '@/router/index.js'
import store from '@/store.js'
import '@/ui-loader.js'
import '@/style/main.scss'
import Request from '@/utils/request.js'
import Scroll from 'vue-slim-better-scroll'

Vue.use(Scroll)

Vue.prototype.$Ajax = Request

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
