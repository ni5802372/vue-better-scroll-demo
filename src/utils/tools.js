import store from '@/store.js';
// 日期格式化
export function dataFormat(value, fmt) {
  let getDate = new Date(value);
  let o = {
    'M+': getDate.getMonth() + 1,
    'd+': getDate.getDate(),
    'h+': getDate.getHours(),
    'm+': getDate.getMinutes(),
    's+': getDate.getSeconds(),
    'q+': Math.floor((getDate.getMonth() + 3) / 3),
    S: getDate.getMilliseconds(),
  };
  if (/(y+)/.test(fmt)) {
    fmt = fmt.replace(
      RegExp.$1,
      (getDate.getFullYear() + '').substr(4 - RegExp.$1.length),
    );
  }
  for (let k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) {
      fmt = fmt.replace(
        RegExp.$1,
        RegExp.$1.length === 1
          ? o[k]
          : ('00' + o[k]).substr(('' + o[k]).length),
      );
    }
  }
  return fmt;
}

// 合并json对像， 不增加属性
export function mergeLeft(obj1, obj2) {
  Object.keys(obj1).forEach(key => {
    if (obj2[key]) obj1[key] = obj2[key];
  });
  return obj1;
}

// url 解析
export function param2Obj(url) {
  const search = url.split('?')[1];
  if (!search) {
    return {};
  }
  return JSON.parse(
    '{"' +
      decodeURIComponent(search)
        .replace(/"/g, '\\"')
        .replace(/&/g, '","')
        .replace(/=/g, '":"') +
      '"}',
  );
}

export const articleConvert = {
  // \r\n换行转 br
  rn2htmlbr: function(str) {  
    str = str.replace(/\r\n/g,"<br/>")
    str = str.replace(/\n/g,"<br/>")
    return str
  },
  // 生成OSS地址
  ossPath: function(writetime, AId, fname){
    const yyyymm = writetime.replace(new RegExp(/(-)/g), '').substr(0, 6)    
    return process.env.VUE_APP_oss_host  + `/articles/${yyyymm}/${AId}/${fname}`
  },
  // 生成头像地址
  avatarPath: function(avatar, thumb = true){
    avatar = Number(avatar)
    let url = ''
    if(avatar < 100){
      url = process.env.VUE_APP_oss_host + '/avatar/' + avatar + '.png'
    }else{
      const groupId = Math.ceil(avatar / 1000) * 1000
      url = process.env.VUE_APP_oss_host + '/avatar/' + groupId + '/' + avatar + '.png'   
    }
    url += '?x-oss-process=style/' + (thumb?'thumb100_100':'avatar')

    if(avatar > 100 && avatar == store.getters.session.avatar){
      url += '&v=' + store.getters.session.time
    }
    return url
  },
  // 图片组排版
  _ImagesGridStyle: {
    img1: null,
    img2: null,
    img3: null
  },
  imageStyle: function(len){
    len = Math.min(3, len)
    if(this._ImagesGridStyle['img' + len] === null){
      let width = store.getters.screen.width
      width = width - 20 * 2 - (len - 1) * 5//左右20， 2，3张图左5
      width = width / len
      let height = width
      if(len === 2) height = Math.floor(height * 0.8)
      if(len === 1) height = Math.floor(height * 0.6)
      this._ImagesGridStyle['img' + len] = {
          width: width + 'px',
          height: height + 'px'
        }
    }
    return this._ImagesGridStyle['img' + len]  
  }
}