import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    screen: { width: 0, height: 0, bar_height: 0, content_height: 0 },
  },
  getters: {
    screen: state => state.screen,
  },
  mutations: {    
    setScreen(state, screen) {
      state.screen = screen;
    },   
  },
  actions: {},
});
